# -*- coding: utf-8 -*-
import telegram
import requests
from bs4 import BeautifulSoup
import time
import datetime


bot = telegram.Bot(token='')

url = "https://bytevirt.com/store/nat-hk-lxc"
products = ["product125"]

products_dict = {
    "product125": "NAT-256-LXC-HK"
}


def get_stock(product):
    resp = requests.get(url)
    soup = BeautifulSoup(resp.text, 'html.parser')

    product_div = soup.find("div", id=product)
    if product_div:
        qty = product_div.find(class_="qty").text
        qty = qty.replace("可用", "").strip()

        if qty.isdigit():
            name = products_dict[product]
            return int(qty), name

    return -1, ""


while True:

    for product in products:
        stock, name = get_stock(product)
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        if stock > 0:
            try:
                bot.sendMessage(chat_id=chat_id, text=f'{name}现在有库存{stock}个!')
            except Exception as e:
                print(f'Error sending telegram msg: {e}')

        else:
            print(f'{name}暂无库存, 当前时间:{now}')

    time.sleep(30)
